$(document).ready(function(){
     
    // ------------------------------------------------------------------------
    // Scrollpane
    // ------------------------------------------------------------------------
    
	$('.scroll-pane:visible').jScrollPane();

    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // tab
    // ------------------------------------------------------------------------

  	$('.tab-area li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab-area li').removeClass('active');
		$('.tab-content').removeClass('show');

		$(this).addClass('active');
		var pane = $("#"+tab_id).addClass('show').find('.scroll-pane');
		pane.jScrollPane();
		var api = pane.data('jsp');
		api.reinitialise();

	})

    // ------------------------------------------------------------------------

});