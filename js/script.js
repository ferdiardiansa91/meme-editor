function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('.detail-container').offset().top - 100;
    if (window_top > div_top) {
        $('.social-sidebar').addClass('sticky');
    } else {
        $('.social-sidebar').removeClass('sticky');
    }
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});