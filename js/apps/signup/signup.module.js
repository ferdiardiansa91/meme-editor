define([
    // Defaults
    "jquery",
    "angular",
    "angularSanitize",
    "jqueryUI",
    "blueimpUpload"
], function($){
    "use strict";

    Date.prototype.getFromFormat = function(format) {
        var yyyy = this.getFullYear().toString();
        format = format.replace(/yyyy/g, yyyy)
        var mm = (this.getMonth()+1).toString(); 
        format = format.replace(/mm/g, (mm[1]?mm:"0"+mm[0]));
        var dd  = this.getDate().toString();
        format = format.replace(/dd/g, (dd[1]?dd:"0"+dd[0]));
        var hh = this.getHours().toString();
        format = format.replace(/hh/g, (hh[1]?hh:"0"+hh[0]));
        var ii = this.getMinutes().toString();
        format = format.replace(/ii/g, (ii[1]?ii:"0"+ii[0]));
        var ss  = this.getSeconds().toString();
        format = format.replace(/ss/g, (ss[1]?ss:"0"+ss[0]));
        return format;
    };

    // ------------------------------------------------------------------------

    var App = {
    	MainApp 		: void 0,

    	init: function(MainApp) {
    		this.MainApp = MainApp;

    		// ------------------------------------------------------------------------
    		// Module
    		// ------------------------------------------------------------------------
    		
    		MainApp.keepoApp.controller('signupModule', ['$scope', '$element', '$http', 'accessToken', 
    			function($scope, $element, $http, accessToken) {
    			$scope.message = {
    				text: void 0,
    				note: void 0
    			};
    			$scope.step = void 0;
    			$scope.previous = false;
    			$scope.validating = false;
    			$scope.data = {
    				username: void 0,
    				email: void 0,
    				password: void 0,
    				confirmed: void 0,
    				dob: void 0,
    				photo: void 0
    			};
                $scope.imageFilename = void 0;
                $scope.imageDataURI  = void 0;
                $scope.processing = {
                    preComplete: false,
                    complete: false,
                    postComplete: false
                };
                $scope.typeDateSupported = Modernizr.inputtypes.date ? 'date' : 'text';

    			var messages = {
    				'username': {
    					text: 'Adek mau daftar pake username apa?',
    					note: 'Min 5 karakter, max 15 karakter, tanpa spasi dan ngga boleh pakai tanda baca (@#!:") ya!'
    				},
    				'email': {
    					text: 'Emailnya dik?',
    					note: ''
    				},
    				'password': {
    					text: 'Masukin passwordnya dik!',
    					note: 'no spasi ya! minimal 6 karakter, max 11 karakter'
    				},
    				'confirm-password': {
    					text: 'Coba ulangi lagi passwordnya!',
    					note: ''
    				},
    				'birthday': {
    					text: 'Tanggal lahir adek?',
    					note: ''
    				},
    				'photo': {
    					text: 'Senyum, lihat kamera dulu ya dik! Bapak mau ambil potonya!',
    					note: ''
    				},
                    'almost-there': {
                        text: 'Bentar ya dik. Bapak buatkan dokumennya dulu',
                        note: ''
                    }
    			};

    			// ------------------------------------------------------------------------
    			
    			$scope.runStep = function() {
    				// Check current step to determine next step (simplest way...to lazy to make it advanced one ¯\_(ツ)_/¯)
    				switch ($scope.step) {
    					case 'username':
    						$scope.step = 'email'; 
    						$scope.previous = true;
    						break;
    					case 'email':
    						$scope.step = 'password';
    						break;
    					case 'password':
    						$scope.step = 'confirm-password';
    						break;
    					case 'confirm-password':
    						$scope.step = 'birthday';
    						break;
    					case 'birthday':
    						$scope.step = 'photo';
    						break;
    					case 'photo':
                            var nDate, nMonth, tmpDate;

                            if (Modernizr.inputtypes.date)
                            { tmpDate = $scope.data.dob; }
                            else {
                                tmpDate = $scope.data.dob.split('/');
                                tmpDate = new Date(tmpDate[2], (tmpDate[1] - 1), tmpDate[0]);
                            }

                            nDate  = ((tmpDate.getDate() < 10) ? '0' : '') + tmpDate.getDate(),
                            nMonth = (((tmpDate.getMonth() + 1) < 10) ? '0' : '') + (tmpDate.getMonth() + 1);

    						// set dob and temp pass (masked it with *)
                            $scope.tempDOB = nDate + '/' + nMonth + '/' + tmpDate.getFullYear();
                            $scope.tempPass = $scope.data.password.replace(/./g, '*');

                            $scope.processing.complete = true;
                            $scope.step = 'almost-there';
                            break;
    					default:
    						$scope.step = 'username';
                            $scope.processing = {
                                preComplete: false,
                                complete: false,
                                postComplete: false
                            };
    						break;
    				};

    				// Set message
    				$scope.message = messages[$scope.step];
    			};

    			$scope.nextStep = function($event, button) {
    				var button = button || false;

    				if ($scope.validating) { return false; }

    				// reset message warning
    				$scope.message.warning = void 0;

    				// Key Enter?
    				if (!button && $event.keyCode == 13) { 
    					this._validate();
    					return;
    				}

    				// Button?
    				if (button) {
    					this._validate();
    					return;
    				}
    			};

    			$scope.prevStep = function($event) {
    				// Check current step to determine prev step (simplest way...to lazy to make it advanced one ¯\_(ツ)_/¯)
    				switch ($scope.step) {
    					case 'email':
    						$scope.step = 'username';
    						$scope.previous = false;

                            $scope.processing = {
                                preComplete: false,
                                complete: false,
                                postComplete: false
                            };
    						break;
    					case 'password':
    						$scope.step = 'email'; 
    						break;
    					case 'confirm-password':
    						$scope.step = 'password';
    						break;
    					case 'birthday':
    						$scope.step = 'confirm-password';
    						break;
    					case 'photo':
    						$scope.step = 'birthday';
    						break;
                        case 'almost-there':
                            $scope.processing = {
                                preComplete: false,
                                complete: false,
                                postComplete: false
                            };
                            $scope.step = 'photo';
                            break;
    					default:
    						break;
    				};

    				// Set message
    				$scope.message = messages[$scope.step];
    			};

                $scope.process = function() {
                    $scope.processing = {
                        preComplete: false,
                        complete: false,
                        postComplete: true
                    };

                    // ------------------------------------------------------------------------
                    // Start upload file

                    if ($scope.imageFilename)
                    { $('#upload').trigger('click'); }
                    else { $scope.completing(); }
                };

                $scope.completing = function(uploadResult) {
                    var prepData = angular.copy($scope.data),    
                        tmpDate;

                    // Set DOB to Y-m-d format
                    if (! Modernizr.inputtypes.date) {
                        tmpDate = $scope.data.dob.split('/');
                        prepData.dob = new Date(tmpDate[2], (tmpDate[1] - 1), tmpDate[0]);
                    }
                    prepData.birthday = prepData.dob.getFromFormat('yyyy-mm-dd');

                    // Set to server
                    $.post(MainApp.baseURL + 'api/v1/auth/register', prepData).
                        done(function(response) {
                            console.log('Great!!! Let\'s redirect the page shall we :)');
                            window.location.href = MainApp.baseURL;
                        }).
                        fail(function(response) {
                            $scope.processing = {
                                preComplete: false,
                                complete: true,
                                postComplete: false
                            };

                            $scope.popuperror = 1;
                            $scope.errormsg = 'Maaf dek, proses registrasi gagal karena ada problem di server. Coba registrasi ulang dong!';

                            $scope.$apply();
                        });
                };

                // ------------------------------------------------------------------------

                $scope._validate = function() {
                    $scope.validating = true;
                    
                    // Username validation
                    if ($scope.step == 'username') {
                        if (! $scope.data.username) {
                            $scope.message = {
                                text: 'Ngantuk ya dik, usernamenya salah atuh?',
                                note: void 0,
                                warning: true
                            }
                            $scope.validating = false;
                            return;
                        }

                        if(! (/^(([a-zA-Z0-9]){5,15})$/g).test($scope.data.username)) { 
                            $scope.message = {
                                text: 'yee... kan sudah dibilangin ngga boleh pake tanda baca dan tanpa spasi.',
                                note: void 0,
                                warning: true
                            };
                            $scope.validating = false;
                            return;
                        }

                        // Check if username already exists?
                        this._isAvailable(
                            // Success
                            function() {
                                $scope.validating = false;
                                $scope.runStep();
                            }, 
                            // Failed
                            function() {
                                $scope.message = {
                                    text: 'Username ini sudah terpakai dik. Coba cari yang lain deh.',
                                    note: void 0,
                                    warning: true
                                };
                                $scope.validating = false;
                            }
                        );

                        return false;
                    }

                    // Email validation
                    if ($scope.step == 'email') {
                        if (! (/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i).test($scope.data.email)) {
                            $scope.message = {
                                text: 'Ngantuk ya dik, emailnya salah atuh?',
                                note: void 0,
                                warning: true
                            }
                            $scope.validating = false;
                            return;
                        }

                        // Check if username already exists?
                        this._isAvailable(
                            // Success
                            function() {
                                $scope.validating = false;
                                $scope.runStep();
                            }, 
                            // Failed
                            function() {
                                $scope.message = {
                                    text: 'Emailnya sudah pernah kedaftar. Coba ganti yang lain deh!',
                                    note: void 0,
                                    warning: true
                                };
                                $scope.validating = false;
                            }
                        );

                        return false;
                    }

                    // Password validation
                    if ($scope.step == 'password') {
                        if (! $scope.data.password) {
                            $scope.message = {
                                text: 'Ngantuk ya dik, usernamenya salah atuh?',
                                note: void 0,
                                warning: true
                            }
                            $scope.validating = false;
                            return;
                        }

                        if (! (/^([^\s]){6,11}$/g).test($scope.data.password)) {
                            $scope.message = {
                                text: 'Minimal 6 karakter, max 11 karakter dan ngga boleh pake spasi!',
                                note: void 0,
                                warning: true
                            };
                            $scope.validating = false;
                            return;
                        }

                        $scope.validating = false;
                        $scope.runStep();

                        return false;
                    }

                    // Confirm Password validation
                    if ($scope.step == 'confirm-password') {
                        if ($scope.data.confirm != $scope.data.password) {
                            $scope.message = {
                                text: 'Lah kok ngga sama? Ulangi lagi deh',
                                note: void 0,
                                warning: true
                            };
                            $scope.validating = false;
                            return;
                        }

                        $scope.validating = false;
                        $scope.runStep();

                        return false;
                    }

                    // DOB Validation
                    if ($scope.step == 'birthday') {
                        var errorMessage = {
                            text: 'Ngawur tanggalnya!',
                            note: void 0,
                            warning: true
                        }, nDate, today = new Date();

                        if (Modernizr.inputtypes.date) {
                            nDate = $scope.data.dob || new Date('invalid date');
                        } else {
                            if (! (/^([0-9]{2}\/[0-9]{2}\/[0-9]{4})$/g).test($scope.data.dob)) {
                                $scope.message = errorMessage;
                                $scope.validating = false;
                                return;
                            }

                            nDate = $scope.data.dob.split('/');
                            nDate = new Date(nDate[2], nDate[1], nDate[0]);
                        }

                        if (nDate == 'Invalid Date') {
                            $scope.message = errorMessage;
                            $scope.validating = false;
                            return;
                        }

                        // Check the year, seriously check the year...sometimes user is really dump ಠ_ಠ
                        if (nDate.getFullYear() > today.getFullYear()) {
                            $scope.message = errorMessage;
                            $scope.validating = false;
                            return;
                        }

                        $scope.validating = false;
                        $scope.runStep();

                        return false;
                    }

                    // Photo Validation
                    if ($scope.step == 'photo') {
                        $scope.validating = false;
                        $scope.runStep();

                        return false;
                    }
                };

    			$scope._isAvailable = function(success, failed) {
    				var url = MainApp.baseURL + 'api/v1/is-available?check=' + $scope.step + '&value=' + $scope.data[$scope.step];

    				// Set message
					$scope.message = {
						text: 'Bentar ya dik, Bapak periksa dulu',
						note: void 0
					}

    				return accessToken.get().then(
    					function (token) {
                            // token received, now get the data
                            return $http.get(url, token).then(
                                function (response) {
                                    success();
                                },
                                function (response) {
                                	failed();
                                }
                            );
                        }
    				);
    			};

                $scope._setTodayDate = function() {
                    var today = new Date(),
                        date  = today.getDate(),
                        month = today.getMonth() + 1,
                        year  = today.getFullYear();

                    if (date < 10) { date = '0' + date; }
                    if (month < 10) { month = '0' + month; }
                };

                $scope._initFileUpload = function($element, options) {
                    var self    = this,
                        options = options || {},
                        fileUploadOptions;

                    if (! $element.length) { return false; }

                    // ------------------------------------------------------------------------
                    
                    fileUploadOptions = {
                        url: MainApp.baseURL + 'api/v1/asset/img',
                        autoUpload: false,
                        dropZone: void 0,

                        add: function(e, data) {
                            var that    = $(this).data('blueimpUIFileupload'),
                                options = that.options,
                                files   = data.files;

                            // Validation
                            // ------------------------------------------------------------------------

                            that._adjustMaxNumberOfFiles(-files.length);
                            data.isAdjusted = true;
                            data.files.valid = data.isValidated = that._validate(files);

                            if (! data.files.valid){
                                that._showError(data.files);
                                return false;
                            }

                            // DOM manipulation
                            // ------------------------------------------------------------------------

                            var reader = new FileReader();
                            reader.onload = function() {
                                $scope.imageDataURI = reader.result;
                                $scope.$apply();
                            };

                            $scope.imageFilename = files[0].name;
                            reader.readAsDataURL(files[0]);

                            $('#upload').one('click', function(e) {
                                e.preventDefault();

                                data.submit();
                            });
                        },

                        done: function(e, data) {
                            // On result
                            // ------------------------------------------------------------------------

                            if (data.result.error) {
                                data.errorThrown = data.result.error;
                                that._trigger('fail', e, data);
                            } else {
                                $scope.data.photo = data.result.id;
                                $scope.$apply();

                                $scope.completing();
                            }
                        },

                        fail: function(e, data) {
                            // Back to step photo
                            $scope.step = 'photo';
                            $scope.message = {
                                text: data.errorThrown,
                                note: void 0,
                                warning: true
                            };
                            $scope.processing = {
                                preComplete: false,
                                complete: false,
                                postComplete: false
                            };

                            $scope.$apply();
                        },

                        progress: function(e, data) {
                            return;
                        }
                    };

                    fileUploadOptions = $.extend(fileUploadOptions, options);
                    $element.fileupload(fileUploadOptions);
                };

    			// ------------------------------------------------------------------------

                var initialize = function (token) {
                    // Set AJAX globally
                    $.ajaxSetup({
                        //headers: { 'Authorization': token.token_type + ' ' + token.access_token }
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', token.token_type + ' ' + token.access_token);
                        }
                    });
                }
                

                // Check access token
                if (window.localStorage.token == undefined) {
                    $.post(self.MainApp.baseURL + 'oauth/access_token', {
                        'grant_type': 'client_credentials',
                        'client_id': 'vkw86HQQ43Qhlf4NQRJLuzd',
                        'client_secret': 'md6qdfnQoXFg7fIlqWWJBqAxdnjFdFx'
                    }).then(function (response) {
                        window.localStorage.token = JSON.stringify(response);

                        initialize(response);
                    });
                } else {
                    initialize(JSON.parse(window.localStorage.token));
                }   


                // Init fileupload
                $scope._initFileUpload($('#upload-images'));

    			// Start step
    			$scope.runStep();
    		}]);

    		// ------------------------------------------------------------------------
    		
    		this.MainApp.keepoApp.config([
			    '$compileProvider', '$interpolateProvider', '$sceProvider', '$httpProvider',
			    function ($compileProvider, $interpolateProvider, $sceProvider, $httpProvider) {
			        // whitelist href content
			        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
			        // disable debug info
			        $compileProvider.debugInfoEnabled(true);
			        // change binding notation
			        $interpolateProvider.startSymbol('<@');
			        $interpolateProvider.endSymbol('@>');
			        // disable SCE
			        $sceProvider.enabled(false);

			        // Set Authorization
                    if (window.localStorage.token) {
                        var temp = JSON.parse(window.localStorage.token);
                        $httpProvider.defaults.headers.common = {
                            'Authorization': temp.token_type + ' ' + temp.access_token
                        };
                    }
			        
			    }
			]);

			MainApp.keepoApp.run();
			angular.bootstrap(document.querySelector("html"), ["keepoApp"]);
    	}
    };

    return App;
});