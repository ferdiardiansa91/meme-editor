define([
	// Defaults
	"jquery",
	"jqueryUI",
	"angular",
	"mediumEditor",
	"mediumInsert",
	"ngTags",
	"spectrum",
	"toc",
	"bootstrap"
], function($, ng, MediumEditor){
	"use strict";

	function extend(b, a) {
	    var prop;
	    if (b === undefined) {
	        return a;
	    }
	    for (prop in a) {
	        if (a.hasOwnProperty(prop) && b.hasOwnProperty(prop) === false) {
	            b[prop] = a[prop];
	        }
	    }
	    return b;
	}

	function titleEditorApp(element, options) {
		this.init(element, options);
	}

	titleEditorApp.prototype = {
		$el : $('.title-holder'),
		defaults: {
			placeholder : 'Title'
		},

		_bind: function() {
			var self = this;

			if (! this.$el.length) { return; }

			// ------------------------------------------------------------------------
			
			this.$el.bind('clickoutside', function() {
				if (self.$el.text().replace(/\s+/, '') == '') {
					$(this).addClass('empty-field');
				}
			});

			this.$el.bind('click', function() {
				$(this).removeClass('empty-field');
			});
		},

		init: function(element, options) {
			this.$el     = element;
			this.options = extend(options, this.defaults);

			// ------------------------------------------------------------------------
			
			this.$el.attr('contenteditable', true).
					 attr('data-placeholder', this.options.placeholder).
					 addClass('title-editor-app');

			if (this.$el.text().replace(/\s+/, '') == '' || ((/^<@/).test(this.$el.text()) && (/@>$/).test(this.$el.text()))) { 
				this.$el.addClass('empty-field');
			}

			// ------------------------------------------------------------------------

			this._bind();
		}
	};

	// ------------------------------------------------------------------------

	var App = {
		MainApp 		: void 0,
		$mainBody 		: void 0,
		editorApp 		: void 0,
		ngApp 			: void 0,
		titleEditorApp 	: titleEditorApp,
		embedURL 		: '//emb.keepo.pyo/',
		uploadURL 		: 'api/v1/asset/img',
		editors  		: {title: void 0, lead: void 0, content: void 0},


		_initEditor: function() {
			var self = this;

			// ------------------------------------------------------------------------
			// Module
			// ------------------------------------------------------------------------

			this.MainApp.keepoApp = angular.module('keepoApp', ['ngSanitize', 'ngTagsInput']);

			// Init controller
			this.MainApp.keepoApp.controller('editorController', ['$scope', '$element', '$http', function($scope, $element, $http) {
				$scope.tempSave = void 0;
				$scope.message  = void 0;
				$scope.tags 	= [];

				$scope.data = {
					title 		: void 0,
					lead 		: void 0,
					content 	: void 0,
					image 		: {
						id 	: void 0,
						url : void 0
					},
					tags 		: [],
					source 		: void 0,
					channel 	: {
						slug : void 0,
						name : void 0
					},
					slug		: void 0
				};

				$scope.data = window.editorData ? $.extend($scope.data, window.editorData) : $scope.data;
				$scope.tags = $scope.data.tags;

				// ------------------------------------------------------------------------
				
				// This for meme in Canvas
				$scope.topcaption 		= '';
				$scope.bottomcaption	= '';

				$scope.setColor 		= '#fff';
				$scope.setOutline 		= '#000';

				$scope.previewGambar =  function(event) {
					var files 	= event.target.files;
					var file 	= files[files.length - 1];
					var size 	= files[0].size;
					$scope.file = file;
					var reader 	= new FileReader();

					console.log(size);

					  if(size > 1024 * 1024) {
				     	$('.alert.alert-danger').fadeIn('fast');
					  }else {
						$('.alert.alert-danger').fadeOut('fast');

						reader.onload = $scope.gambarLoad;
						reader.readAsDataURL(file);
					    $('.remove-image').fadeIn('slow');
						$('.eb-meme-upload-holder').fadeOut(1000);
					}
				}

				$scope.hapusGambar = function(e) {
					$('.preview').attr('src','');
					$('.remove-image').fadeOut('fast');
				    $('.eb-meme-upload-holder').fadeIn('fast');

				    $scope.topcaption	 = '';
				    $scope.bottomcaption = '';
				}

				$scope.gambarLoad = function(e) {
					$scope.$apply(function() {
						$('.preview').attr('src', e.target.result);
					});
				}

			}]);
			
			// Ini buat color
			this.MainApp.keepoApp.directive('color', function() {
				return {
			        restrict: 'E',
			        require: 'ngModel',
			        scope: false,
			        replace: true,
			        template: "<span><input class='input-small' /></span>",
			        link: function(scope, element, attrs, ngModel) {
			            var input = element.find('input');
			            var options = angular.extend({
			                color: ngModel.$viewValue,
			                move: function(color) {
			                    scope.$apply(function() {
			                      ngModel.$setViewValue(color.toHexString());
			                    });
			                }
			            }, scope.$eval(attrs.options));
			            
			            ngModel.$render = function() {
			              input.spectrum('set', ngModel.$viewValue || '');
			            };
			            
			            input.spectrum(options);
			        }
			    };
			});

			// Ini buat outline
			this.MainApp.keepoApp.directive('outline', function() {
				return {
			        restrict: 'E',
			        require: 'ngModel',
			        scope: false,
			        replace: true,
			        template: "<span><input class='input-small' /></span>",
			        link: function(scope, element, attrs, ngModel) {
			            var input = element.find('input');
			            var options = angular.extend({
			                color: ngModel.$viewValue,
			                move: function(color) {
			                    scope.$apply(function() {
			                      ngModel.$setViewValue(color.toHexString());
			                    });
			                }
			            }, scope.$eval(attrs.options));
			            
			            ngModel.$render = function() {
			              input.spectrum('set', ngModel.$viewValue || '');
			            };
			            
			            input.spectrum(options);
			        }
			    };
			});
			
			this.MainApp.keepoApp.directive('draggable', function() {
			  return {
			    restrict:'A',
			    link: function(scope, element, attrs) {
			      element.draggable({
			        axis 			: 'x y',
					containment		: "#preview-gambar"
			      });
			    }
			  };
			});


			// Wait until angular finished rendering ¯\_(ツ)_/¯
			setTimeout(function() {
				// Title
				if (self.$mainBody.find('.eb-title').length) {
					var titleEditor = new titleEditorApp(self.$mainBody.find('.eb-title'), {
						placeholder: 'Title'
					});

					self.editors.title = titleEditor;
				}

			}, 50);

			

			// ------------------------------------------------------------------------

			this.MainApp.keepoApp.config([
			    '$compileProvider', '$interpolateProvider', '$sceProvider', '$httpProvider',
			    function ($compileProvider, $interpolateProvider, $sceProvider, $httpProvider) {
			        // whitelist href content
			        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
			        // disable debug info
			        $compileProvider.debugInfoEnabled(true);
			        // change binding notation
			        $interpolateProvider.startSymbol('<@');
			        $interpolateProvider.endSymbol('@>');
			        // disable SCE
			        $sceProvider.enabled(false);

			        // Set Authorization (Assuming token already stored in localStorage)
			        var temp = JSON.parse(window.localStorage.token);
			        $httpProvider.defaults.headers.common = {
			        	'Authorization': 'Bearer ' + temp.token
			        };
			    }
			]);

			// start up the module
			if (! this.editorApp || (this.editorApp == 'meme')) {
				this.MainApp.keepoApp.run();
				angular.bootstrap(document.querySelector("html"), ["keepoApp"]);
			}
		},

		// ------------------------------------------------------------------------

		init: function(MainApp) {
			var self = this;

			// Initialize variables
			this.MainApp        = MainApp;
			this.$mainContainer = $('.main-container');
			this.$mainBody      = $('.editor-container');
			this.editorApp      = this.$mainBody.data('editor');
			this.uploadURL 		= MainApp.baseURL + this.uploadURL;

			// Initialize editor
			if (! self.editorApp || (self.editorApp == 'meme'))
			{ self._initEditor(); }
			else {
				require(['apps/editors/' + self.editorApp + '.module'], function(EditorApp) {
					EditorApp.init(self);
				});
			}
		},
	};

	return App;
});